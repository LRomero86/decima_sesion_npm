//usamos moment

var moment = require('moment');

// fecha actual
var now = moment(new Date());

// fecha UTC

var utc = moment(new Date()).utc();

console.log('actual', now);
console.log('utc', utc);

var diferencia = now.hours() - utc.hours();

console.log('diferencia', diferencia);

//muestro ambas fechas

console.log(now.format('DD mm YYYY'));
console.log(utc.format('hh:mm:ss'));

var duration = new moment(utc.diff(now));

var primera_fecha = moment([2021, 07, 07]);
var segunda_fecha = moment([1986, 10, 04]);

// diferencia de días

var result_dias_diff = primera_fecha.diff(segunda_fecha, 'days');
console.log('N° de días: ', result_dias_diff);

// diferencia en horas

var result_horas_diff = primera_fecha.diff(segunda_fecha, 'hours');
console.log('en horas: ', result_horas_diff);

//valor absoluto

var duration = moment.duration(primera_fecha.diff(segunda_fecha));
console.log('duración: ', duration);